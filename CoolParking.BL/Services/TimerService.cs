﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System;
using System.Threading;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService, IDisposable
    {
        public double Interval { get; set; }
        private System.Timers.Timer _timer;
        
        public TimerService ()
        {
            _timer = new System.Timers.Timer();
            _timer.Elapsed += _timer_Elapsed;
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var tmp = Volatile.Read(ref Elapsed);
            if (tmp != null)
                Elapsed.Invoke(sender,e);
        }

        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            try
            {
                _timer?.Dispose();
            }
            catch
            {
                throw new Exception();
            }
        }

        public void Start()
        {
            _timer.Interval = Interval;
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }
    }
}