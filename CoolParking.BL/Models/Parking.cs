﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
    public class Parking:IDisposable
    {
        private static Parking _instance;
        public decimal Balance { get; set; }
        public List<Vehicle> Vehicles { get; set; }
        private Parking()
        {
            Vehicles = new List<Vehicle>();
        }

        public static Parking GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Parking();
            }
            return _instance;
        }

        public void Dispose()
        {
            Vehicles = null;
            _instance = null;
        }
    }
}