﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal STARTBALANCE = 0;
        public const int MAXCAPACITY = 10;
        public const int PAYMENTPERIOD = 5;
        public const int LOGEDPERIOD = 60;
        public const decimal VEHICLECARTARIFFS = 2;
        public const decimal VEHICLETRUCKTARIFFS = 5;
        public const decimal VEHICLEBUSTARIFFS = 3.5m;
        public const decimal VEHICLEMOTORCYCLETARIFFS = 1;
        public const decimal FINERATE = 2.5m;

        
    }
}